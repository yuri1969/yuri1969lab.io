---
layout: post
title:  "TLS cipher suites"
categories: [tls]
---

# TLSvX

Existence of different versions of the TLS protocol is not a surprise. During establishing of a TLS session both client and server must agree on a common TLS cipher suite. Yet, there are *many* compatibility-related issues beyond a simple TLS version check.

The most common one seems to be disabling (not supporting) cipher suites within given TLS version. This means client fails at establishing a TLS session since there is no common cipher suite found.

## When does it matter?

Upgrading any TLS-enabled server software brings the risk of changing the list of server-supported cipher suites. Similarly, disabling cipher suites deemed unsafe changes the list. This might lead to effectively firewalling all clients which have been relying on the disabled cipher suites.

Use of TLS is common and widely-spread. It ranges form simple webservers (*nginx*, *caddy*) to highly specialized apps using custom protocols on top of TLS. Impact of changing the cipher suite differs accordingly.

Disabling an unsafe cipher suite at a public-facing webserver is relatively safe since the targeted clients consist of (mostly) up-to-date web browsers. Other deployments might be way trickier.

For example, machine-to-machine communication in the B2B field. A B2B server usually serves a number of 3rd party clients. One can't simply disable a cipher suite since clients vary greatly - different software/platforms of different age. B2B systems based on 15 years old Java platform are still there.

Analyzing the set of actively used cipher suites before any change is the way to go.

## Other dark corners

TLS cipher suite naming might catch an unsuspecting subject off guard. There are at least three naming schemes used - *OpenSSL*, *GNU TLS*, and *Internet Assigned Numbers Authority* (IANA). There is even a website dedicated to that inconsistency: [ciphersuite.info][info].

Third party services/apps usually do not advertize their supported cipher suites beforehand. This means one must use a [utility][util] to get the actual suite list.

Order-related problems might be encountered since the ordering logic of server-listed TLS cipher suites reflects the suite preference. This means disabling a cipher suite can trigger a bug related to cipher ordering or concerning the "next picked" suite.

[util]: https://superuser.com/a/224263
[info]: https://ciphersuite.info/
