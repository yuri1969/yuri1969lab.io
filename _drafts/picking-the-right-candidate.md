---
layout: post
title:  "Picking the right candidate"
categories: [workflows, sw]
---

# Which is the right one?

Want to replace an outdated dependency? Need to use a new tool? Such choice should never be made blindly. More so when the change is gonna affect the productive environment.

What about situations when the *thing* to be replaced is *THE* mission critical component. That replacement simply has to be able to make you $$$ for the next years to come. So one should pick wisely, right?

There are usually multiple different alternatives having different qualities. Avoiding the decision paralysis can be hard in case the scope is large. There must be a set of metrics established to evaluate the alternatives. Which are the key metrics to base the decision on?

## Features

Actual required features should come first in the decision making. However, things can get complex when requirements include a dozen of orthogonal features. Fitting all selected alternatives can be challenging. More so when there is no solution ticking all the boxes. A feature matrix having weighted requirements usually helps. However, setting the weights is not trivial - *"Is having X really more important than being able to use Y?"*

The outcome often includes more than one solution achieving a very similar score. Those providing a similar feature set are to be further evaluated.

## License

Commercial solutions having backing by a *real* business entity providing references, support, etc. This sounds like a rock-solid solution fit for production but... There is a downside.

Compared to the open-source software (OSS) the majority of closed-source is fully opaque. Encountering even a tiniest bug or a need for a new feature requires begging the supplier to act. You simply can't do it yourself - there is no place for a contribution.

Having no source code gets in the way in cases where is a need for an insight - solving performance issues or unexpected behavior. Additionally, commercial software often come with annoying requirements, like having to deal with *license files*, or *license renewals*. All those interfere with you day-to-day work - have you ever waited weeks for a requested license file just to receive a broken one?

The OSS seems like the natural way to go - no licensing hassle. But the OSS licenses are many. Some of them bring odd consequences (mainly for SaaS deployments).

Tricky OSS licenses:

* **GPL** - The SW you distribute becomes GPL-licensed at the moment you link it to any GPL-licensed code. It was designed to be this powerful to require the users to contribute changes.
* **LGPL** - The same as **GPL** unless you just treat the **LGPL** SW as a library (no static linking, etc.).
* **AGPL** - The same as **GPL** with an explicit mention of *SaaS*/web distribution.

Safe-to-use licenses:

* **Apache 2.0**
* **BSD**
* **MIT**

## Project's age

Every dependency resides in a phase of the SW development lifecycle.

1. **Inception** - pretty busy - bugs get fixed and many features added; nobody really uses it in production
1. **Maturity** - widely adopted to production; bugs still get fixed; features get added
1. **Decline** - overall adoption declines; mostly critical bugs get fixed; no new features get added
1. **Abandonment** - existing users are advised to migrate away; almost no bugs nor features get worked on

For your project, you would ideally want a mature project - the others are simply less desirable. However, what if there is no such option available? Should you pick up a promising young project which still might die off/get bought/shift its direction in a year or rather a stalled declining but somehow still used one? That's a tough pick.

## Community

Do others actually use that *thing*? If so it gives you a bit of trust in the tech and also ease troubleshooting when needed - think of answered [SO][so] questions, random googling, etc.

The notorious GitHub stars are often taken as the main *popularity metric*. Often, a project with almost zero issues/PRs/forks somehow presents itself with thousands of stars. The stars can be gained simply by using popular channels like [HN][hn], [Reddit], [medium.com][medium], [Twitter/X][twitter], or similar sites to gain visibility of the project. But visibility doesn't imply a large number of actual users. This metric reflects how many users have been *somehow interested* in the project - nothing more.

Number of forks, code frequency, or number of recently resolved issues seem to be better metrics.

## Misc risks

One should always apply the infamous [bus factor][busf]. There have been many stellar projects driven by a single person - like the early days of [Elasticsearch][es] - but is the risk really wroth it?

Picking a dependency written using a mainstream language is way less risky than using an obscure one. Need to fix a bug? Good luck finding someone daring to deal with Lisp/Haskell or old Perl hacks. The same applies to horribly written code bases.

Relying on projects based in unstable countries. For example, there are many projects backed by one of those big Chinese companies. Those projects *might* be affected by current wobbly US-China relations.

[busf]: https://en.wikipedia.org/wiki/Bus_factor
[es]: https://github.com/elastic/elasticsearch
[so]: https://stackoverflow.com
[hn]: https://news.ycombinator.com/
[reddit]: https://reddit.com/
[twitter]: https://twitter.com/
[medium]: https://medium.com/
