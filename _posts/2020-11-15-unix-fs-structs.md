---
layout: post
title:  "UNIX FS structs"
categories: [linux, fs]
---

# Structures of UNIX&Linux FS

In day-to-day life one deals with elementary tools like **mv** or **ls**. In some situations it might be beneficial to know what lies underneath - how do the "files" interact? Following description tries to capture only the main ideas. The implementation/naming differs among different UNIXes, Linux, and others.

## Basic Components

In UNIX world the FS is composed of files - sequences of bytes - and directories. This feels pretty natural. There are also special types of files like *[device files][dev]* - providing access to various resources, *[sockets][socket]* - an abstraction of data streams, or *[named pipes][pipe]* - providing means of IPC.

### Low level

At the level close to physical storage device the data is abstracted to units called *disk block*. The hardware (SSD, NVM) providing the storage is thus referred as a *block device*. Usually, each such disk block holds 4KiB of data.

### Indexing

The UNIX/Linux FS (or better [VFS][vfs]) generalizes all files and directories too(!) to numerically indexed structure called *inode*. This structure keeps its sequential index number, holds/references file "physical" data as pointers to disk blocks. It also encapsulates file attributes (size in bytes, owner, UNIX permissions, file-related timestamps, etc.). The block addressing is done in a clever way that "small files" are referenced directly inside an inode and "large ones" are referenced indirectly using multiple levels of references using so-called *indirect blocks*. The FS usually stores all inodes in an *inode table*.

It's worth mentioning, the inode indices are unique only within a single FS (e.g. partition) - the further text is related to a single FS only.

Nonintuitively, inode attributes do not contain file name...

### Hierarchy

Files and directories usually have a name so it has to be stored somewhere. That is achieved using directories. Directory is implemented just as a regular inode which contains a filename-to-inode number mapping inside its disk blocks (called *directory block*). The directory to file inode reference is called a *hardlink*. So directories are files - *[directory files][dirfile]*. This way, only the parent directory "is able to get" its contained files/directories by name.

This design imposes a clear separation of naming/hierarchy and data-related information.

### Runtime

Any regular user-space process can interact with the files through means of VFS. For each successfully interacted file the process obtains a new *file descriptor* from OS. The system keeps active FDs for each process as indices of process' *file descriptor table*. The FD table holds a reference to system's *open file table* which holds metadata describing the interaction type, file cursor position, and also a *vnode table* index.


The vnode table holds *vnode* structures which act more or less as an in-memory copy of a file inode with additional information helping in faster FS lookup. This additional level provides caching of "physical" inodes. The implementations differ a bit there since there are usually another caching structures - such as Linux's *[dentry][kernelvfs]*. So... all this finally allows mapping processes manipulating the files to the disk blocks.

~~~~

                         process table                                   vnode table              inode
                      +-----------------+                             +----------------+      +-----------+
     process          |       ...       |                             |      ...       |      | 608       |
  +------------+      +-----------------+       open file table       +----------------+      |           |
  | PID 101    |      | PID 101         |        +------------+       |  regular_file  |  +---+   user1   |       disk block
  |            |      |                 |        |    ...     |  +-->8|                |  |   |           |      +----------+
  | int fd = 5 +--+   |     FD table    |        +------------+  |    | +------------+ |  |   |   group   |      |          |
  |            |  |   |  +-----------+  |   +-->4| READ, 2, 8 +--+    | | copy of    | |  |   +-----------+      |          |
  +------------+  |   | 0|           |  |   |    +------------+       | | inode 608  | |  |   |   data    |  +-->+   data   |
                  |   |  +-----------+  |   |    |    ...     |       | |            +----+   +-----------+  |   |          |
                  |   | 1|           |  |   |    +----------- |       | |            | |      |           +--+   |          |
                  |   |  +-----------+  |   |    | WRITE, 0   |       | |            | |      +-----+-----+      +----------+
                  |   |  |    ...    |  |   |    +----------- |       | |            | |            ^
                  |   |  +-----------+  |   |    |    ...     |       | +------------+ |            |
                  +---->5|     4     +------+    +------------+       +----------------+            +---------+
                      |  +-----------+  |                             |      ...       |          inode       |
                      |  |    ...    |  |                             +----------------+      +-----------+   |
                      |  +-----------+  |                                                     | 712       |   |
                      +-----------------+                                                     |           |   |
                      |       ...       |                                                     |   user1   |   |
                      +-----------------+                                                     |           |   |
                                                                                              |   group   |   |
                                                                                              +------+----+   |
                                                                                              |file1 | 608+---+
                                                                                              +------+----+
                                                                                              |           |
                                                                                              +-----------+
~~~~

## Interactions

Execution of utils like **mv**, **ln**, or **rm** have an effect on a very important part of the described structures - *reference counts*. Many of the listed structures types have to be deallocated when they have no use anymore. In more detail, an open file table entry should be deallocated when the pointing FD gets closed, a vnode table entry is useless when there it's not being referenced by an open file table entry, a file's inode should be referenced by a directory inode in order to be reachable.

So the vnode table entries, open file table entries, and inodes have a reference count field defined. Usually, when the reference count reaches zero the structure can be deallocated.

Let's illustrate a bit:

* A basic [hardlinking][hardlink] using **ln** simply creates a new entry in directory's inode and increases reference count in file's inode.
* Moving an existing file to a new destination via **mv** creates an entry in destination directory's inode (a hardlink) and deletes one in the source one - file's inode reference count stays the same. Additionally, this means changing file location doesn't affect any user space process manipulating that file.
* Calling **rm** on a file removes a directory's inode entry and decrements file's inode reference count. If the file's inode reference count was lowered to zero it waits to be discarded as soon as there is no related vnode table entry ([no process using that file][unlink]).

[dev]:https://www.kernel.org/doc/html/v4.11/admin-guide/devices.html
[socket]:https://linux.die.net/man/7/socket
[pipe]:https://linux.die.net/man/7/pipe
[vfs]:https://www.kernel.org/doc/html/latest/filesystems/vfs.html
[dirfile]:https://www.nongnu.org/ext2-doc/ext2.html#inode-table
[kernelvfs]:https://www.kernel.org/doc/html/latest/filesystems/vfs.html#introduction
[hardlink]:https://linux.die.net/man/3/link
[unlink]:https://linux.die.net/man/3/unlink
