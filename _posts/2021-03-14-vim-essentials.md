---
layout: post
title:  "Vim essentials"
categories: vim
---

# Vim is visual!

First of all, use the **h**, **j**, **k**, **l** instead of the arrow keys. Vim is intended to be used that way – it minimizes the precious finger movement.

List of essential actions:

- **^** move to line start
- **$** move line end
- **w** move to start of the next word
- **b** move to start of the previous word
- **a** increase cursor position and enter *the Insert mode*
- **i** enter *the Insert mode*
- **x** delete the char at the current position
- **Control** + **f** move page down
- **Control** + **b** move page up
- **gg** or **:1** skip to the first line
- **G** skip to the last line
- **:\<number\>** skip to the given line
- **.** repeat the last action
- **u** undo
- **Control** + **r** redo
- **J** concat the current line with the following one
- **\<number\>**-prefixed *Normal mode* action gets repeated (**5x** deletes 5 chars)
- **y** in *the Visual mode* copies the selection
- **d** in *the Visual mode* copies and deletes the selection
- **yy** copy the current line
- **dd** delete the current line
- **p** paste after the current cursor position
- **P** paste before the current cursor position
- **/** search following
- **?** search preceding
- **n** next search result
- **N** previous search result
- **\*** search the current word
- **:noh** clear search results
- **:%s/\<regex>/\<replacement\>/** substitute, replace just the first occurrence on a line
- **:%s/\<regex>/\<replacement\>/g** substitute, replace all occurrences on a line
- **:g/\<regex>/d** delete all matching lines
- **:!g/\<regex>/d** delete all non-matching lines
- **\>\>** add a Tab equivalent (see [**expandtab**][tab]) to the current line
- **\<\<** remove a Tab equivalent from the current line
- **>** in *the Visual mode* adds a Tab equivalent to the selected lines
- **<** in *the Visual mode* removes a Tab equivalent from the selected lines
- **:set number** show line numbering
- **:set nonumber** hide line numbering
- **:%!xxd** edit file in hexa
- **:set fileformat=\<unix\|dos\>** set EOL
- **:set encoding=\<encoding\>** display file using given encoding

[tab]: https://vim.fandom.com/wiki/Converting_tabs_to_spaces
