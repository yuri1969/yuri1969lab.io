---
layout: post
title: "Eclipse woes"
categories: [rant, ide]
---

# List of Eclipse woes

Eclipse IDE is free, [open-source][epl], serves as a basis for many [Eclipse-based tools][tools], and uses a lot of memory as all full-fat IDEs do. However, using it on regular basis brings a lot of pain. There even used to be a dedicated website called [ihateeclipse.com][hateeclipse].

Here goes my list:

- General speed of essential tasks - code completion often features a one second lag. Also, context
  menus show with a noticeable delay.
- Leaning towards an unresponsive state - opening Git history while updating a Maven repo? Nope
- Crashing on trivial tasks. One of hundreds plugins is usually the culprit, yeah
- Being forced to use multiple perspectives induces annoying and often slow full window reloads
- Rendering of various UI parts getting randomly corrupted. Minimize, maximize, refresh to revive, OK...
- Huge bloated margins being around every UI element eating precious usable space
- Having a blank page with **The resource is out of sync with the file system. Press F5 to reload**
  on files modified not-in-Eclipse, what!?
- Employing insanely murky terms. Using word **Team** for such a basic thing as **Version Control System**
  (VCS), why?
- Being a Java-first IDE but Maven/Gradle plugins(!) feel like obstacles
- Plugins are plenty although many feeling unmaintained and/or stuck in _beta_ at best
- The dark theme styling being still very sad. Also completely non-styled UI elements still popping once
  a while. The [initial 2014 one][dark] was really horrible
- Insisting on EGit/JGit. That being slow, featuring one of the weirdest unintuitive Git UIs, and
  mixing controls for everyday and one-off actions together
- Actually storing its global(!) IDE-related config inside a _workspace_, not globally. Export/Import -
  General - Preferences. Easy, right?
- Line numbers being off by default till 2013. This is telling
- Background running tasks feature a cancel button, although in most cases it does not actually cancel
  the task.
- Installing new or updating existing plugins being incredibly slow process. Moreover, Eclipse is essentially
  a bunch of plugins?
- Confusing status messages like **Initializing Java Tooling**. What is Java Tooling?
- Versioning. The IDE itself has at lest two _versions_. One is the release codename - 'Juno', 'Mars', 'Neon', or since 2018 the more sane 'YYYY-MM'. Other is the semantically versioned platform version. Why?

[epl]: https://www.eclipse.org/legal/eplfaq.php
[tools]: https://en.wikipedia.org/wiki/List_of_Eclipse-based_software
[hateeclipse]: https://web.archive.org/web/*/http://www.ihateeclipse.com
[dark]: https://www.eclipse.org/community/eclipse_newsletter/2018/june/darktheme.php
