---
layout: post
title:  "Workflow engines"
categories: [workflows]
---

# Workflow engines in '20-23

There is a well-maintained called [awesome-workflow-engines][awe] which gathers existing workflow engine projects. When one wants to get the experience past polished demos, the hand-on is the right way. Even though there are nice [comparisons available][cmp]...

Offline (cron & ETL) vs online stream (events) processing vs Continuos Deployment - completely different use cases.

Workflow definition via regular code vs DSL vs BPMN - *developer*-oriented vs *business*-oriented automation.

Means for interaction with running/failed workflows - are there tools for monitoring and/or interaction?

## JBoss jBPM

- the original Red Hat BPMN compliant engine - clearly business-oriented & questionable complexity scaling
- traditional Java + RDBMS stack
- business logic written in Java snippets or encapsulated to Java-based services
- workflow run visualization as BPMN

## Activiti

- ex-Red Hat employees's lessons-learned from jBPM
- BPMN compliant workflow definitions
- aims to be easier to integrate compared to jBPM
- business logic treated similarly to jBPM

## Camunda

- an Activiti fork - traditional Java + RDBMS stack
- BPMN compliant workflow definition
- human tasks/workflow steps - users filling web forms
- business logic treated similarly to jBPM with addition of external long-polling workers
- EE provides UI, etc.

## Zeebe/Camunda 8

- horizontally scalable next-gen of Camunda
- non-standard *source available* license
- based on a combo of RocksDB and the Raft protocol - event sourcing - eventual consistency creep
- history log exported to Elasticsearch/Apache Kafka
- Java-based with clients in Java, Go, and more
- business logic performed by external polling workers over gRPC
- EE provides production-ready addons and an extensive UI

## Flowable

- an Activiti fork - traditional Java + RDBMS stack
- business logic treated similarly to jBPM with addition of external polling REST workers
- EE provides improved deployments, UI, etc.

## Kogito

- Red Hat owned next-gen of jBPM
- horizontally scalable architecture of microservices performing business logic
- CLI + Quarkus services + framework of many addons (e.g. Apache Kafka-based events or persistence)
- early stages of its production ready state?
- limited UI

## Netflix Conductor

- JSON workflow definitions tend to be confusing
- first-class citizen for the state store is *Dynomite* - Netflix's Dynamo wrapper for Redis
- WiP works on stabilizing alternative state stores (e.g. RDBMS-based)
- many edge cases hitting eventual consistency - Apache Zookeeper required?
- business logic performed by external polling workers
- workflow run visualization

## Orkes Conductor

- ex-Netflix employees' SaaS fork
- viability vs the original is uncertain

## Uber Cadence

- ex-AWS Simple Workflow (SWF) people
- workflow definition is workflow-as-a-code (runnable Java/Go program) - developing a workflow requires a Java/Go dev
- each workflow definition inherently contains boilerplate code
- parameters are typed
- classic unit tests are available
- allows dynamic flows generated on the fly
- Go-based with Java and Go clients
- Apache Cassandra used for the state store - event sourcing?
- performance scaling problems?
- business logic performed by workers long-polling over gRPC
- *Signals* used to interact with running workflows
- limited UI
- no workflow run visualization

## Temporal

- ex-Uber employees' fork of Uber Cadence
- tech stack upgrades vs Uber Cadence

## Apache Airflow

- ETL-heavy batch data processor - long running heavy workflows - very far from streaming
- workflow definition is workflow-as-a-code in Python
- lacks support of dynamic behavior like starting a workflow with an API call or passing parameters
- heavily reliant on the main scheduler service
- different *Executors* implementations (local, K8s, Celery, etc.) providing different environment for business logic
- workflow run visualization

## Prefect

- Apache Airflow done better
- workflow definition is composed form granular workflow-as-a-code Python pieces
- workflows can be parametrized or triggered via webhook unlike in Apache Airflow

## Apache DolphinScheduler

- predominantly Chinese-based - WiP English lang efforts
- scalable Apache Airflow in Java - multiple primary nodes & worker nodes, Apache Zookeeper, and a RDBMS for history log
- drag-n-drop visual workflow definition
- supports API calls, built-in connectors to the Apache ecosystem, etc.
- workflow run visualization

## Argo Workflows

- heavily reliant on K8s - implemented as K8s Custom Resource Definition (CRD)/Operators/Kubernetes Jobs
- workflows defined in (K8s-ish) YAML templates
- every workflow step spins a separate K8s pod - not viable for short/latency-sensitive steps
- K8s API server seems to be the obvious bottleneck
- a massive stack of tight K8s integration, AWS S3 (or so) for data store, RDBMS for history log, etc.
- deployment somehow pushed via a separate product - *Argo CD*
- workflow run visualization oddly omitting not yet reached steps

## AWS Step Functions

- continuation of AWS SWF
- fully serverless
- pay-as-you-change-workflow-state model leads to "merging" workflow steps together
- the typical case is to conditionally invoke a bunch of custom AWS Lambdas wired via AWS SQS
- very low-level - limited means of interaction
- often requires a custom implementation of basic functionality
- workflow run visualization

## Apache NiFi

- ETL orientated at streamed data - ingest-transform-push
- Java-based - originated in the NSA
- multiple instances can be employed - a cluster managed by Apache Zookeeper
- drag-n-drop visual workflow definition - *Processors* chained with backpressure-enabled *Connections*
- workflow run visualization
- built-in connectors to the Apache ecosystem
- performance scaling issues?

## Kestra

- Java-based - originated as an attempt to properly replace Apache Airflow
- horizontally scalable event-based architecture of based on RDBMS/Apache Kafka
- primary persistence & history log is RDBMS/Elasticsearch-based
- custom YAML-based workflows definition
- workflow run visualization
- first-class support for container-native (Docker), shell, Python, Node.js, etc. workflow steps
- built-in connectors to various 3rd party services
- business logic encapsulated to custom plugins - workflow steps
- business logic encapsulated to *custom plugins* - workflow steps
- heavy reliance on Kafka Streams

[awe]: https://github.com/meirwah/awesome-workflow-engines
[cmp]: https://medium.com/@chucksanders22/netflix-conductor-v-s-temporal-uber-cadence-v-s-zeebe-vs-airflow-320df0365948
