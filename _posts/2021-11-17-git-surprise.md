---
layout: post
title:  "Another Git surprise"
categories: [rant, git]
---

# World is multiple platforms

As a developer you can choose from multiple operation systems. Everybody can pick the most fitting environment - that's nice. Major tools can be ran at any of those. No surprise. Git, as the *de facto standard* [CVS][cvs] tool, can be ran on Linux, macOS, or Windows. Again, that's cool.

## Projects can span multiple platforms

Now imagine a project being developed and ran (for years) solely on Linux. Everything is great. The project grows and flourishes.

A new dev joins the team. After some time the dev is tasked to perform an urgent change in a repo which effectively holds configuration of a runtime environment. The repo hasn't been cloned to his machine yet.

As soon as the dev clones the repo, he/she knows something is terribly bad - the fresh workspace immediately contains changes in many of its files.

What happened? The Git index simply tried to present file paths created on a case-sensitive platform on a case-insensitive platform. There are distinct files treated as not distinct using a case-insensitive platform. This causes all the workspace changes.

On his Windows machine, the new dev cloned the repo previously exclusively developed on Linux. Yes, **process-date-Y.conf** file is treated the same as **process-date-y.conf** on Windows...

## Simply don't

The best solution is to avoid case sensitive file paths in Git indices altogether. This way it's possible to make the experience less painful for everyone involved.

[cvs]:https://en.wikipedia.org/wiki/Concurrent_Versions_System
