---
layout: post
title:  "Event driven horrors"
categories: [rant, sw]
---

# Are events any good?

Event-driven architectures (EDA) seems to been hyped for years. It might appear like silver bullet to deal with any kind of application - a web shop, a manufacturing management, or banking system - no problem. However, under all the shiny statements about scalability, decoupling, domain-specific development, etc. there are massive obstacles which can't be seen at first glance.

A text book example is a nice example of a super cool application dealing with the serious topic of processing website clicks. Easy. There are tons of such fire-n-forget examples. Gather events, process events, write events, finish. That's cool. But do those examples care when 10 events go missing? Usually, they do not.

How about applying EDA to a bog-standard application - not a fire-n-forget one.

## Administration/Ops

Day-to-day business running any non-trivial application involves solving issues. Error notifications and incident reports must be dealt with. Things like bugs, infrastructure failures, misconfigurations, or non-sanitized bad input simply happen. Even in an event-driven app, there must be a way to fix those issues and remedy all the side-effects. This usually involves *re-playing* the events. However, it might also involve modifying the *poison* (broken) events and then sending their fixed copies again.

Those operations are usually done by administrators of the system. Those admins must be able to query and pick events from a queue/topic/? and move them somewhere else. Sometimes a modification of the event or even emitting ones ad-hoc is needed to remedy the problem. All this while being able to reason about the event sources, event destinations, and event content. A thorough documentation of the entire *event flow* is a must.

These manual operations typically result in creating an ad-hoc repair script/utility. But as the time goes there utilities grow in size and number... a second full-blown *management* application often emerges in parallel to the main one.

So one has to maintain two applications instead of one.

## Observability

Similarly to other pathologically disaggregated architectures, such as microservices, reasoning about a unit of work being done by the system is impossible without investing in work tracing. Path of each event going through the system must be tracked, stored, and presented in a human understandable way.

This sounds like an easy task, but the complexity often goes over the top. There are event flows through the system where an event splits to many others which can be joined back together, split again, etc.

The more logging/tracing the better rule applies here. However, even with a relatively modest event throughput the amount of logging events/traces increases rapidly. This brings really tough decisions about the log retention policy. This also leads to curious questions from the business people like: *"Are you telling me, you don't know what happened last month?"*

In the context of business critical applications, the traces are first-class citizens - they must be delivered in a reliable way. This implies having log delivery retry policies, persistent log delivery queues, and log non-event alerting in place. These techniques are required to make it possible to reliably answer even basic questions like: *"Where did request X fail?"*

## Persistence

Like in every distributed system, event-driven apps must assume the network is unreliable by nature. Add that to the classic multi-process behavior - clients go down, K8s pods get restarted, etc. Handling such environment in a reliable fashion is complex.

Basically, one must persist produced events first and then attempt to deliver them. Since non-persisted events would be dropped in case of a restart of a producer trying to repeatedly deliver them during a network outage. Therefore producers implement their own kinda of a local persisted queue of outbound events.

## OOTB brokers

Nowadays messaging/event broker technology is plentiful. Even fully-managed solutions are provided by major cloud vendors. So it might seem nothing surprising would lurk in the shadows. Oh well... The brokers are still an extremely complex piece of clustered software. There are usually tens (100s?) of configuration parameters which drastically affect the whole system. On top of that there are often additional highly configurable libraries built on top of the brokers (e.g. [Kafka Streams][kstreams]).

First of all, majority of the brokers and clients is by default configured to favor event throughput to reliability - no ACKs required and no persistence. Why? Big throughput benchmark numbers look great in PR slides, or fire-n-forget applications. That's it.

The basic thing is to completely invert the default setup of both clients and brokers to prefer reliability. Usually, one would still miss a few config knobs since there is always something special.

* Clustered brokers not voting in a consistent manner by default?
* Kafka offsets [deleted after 24h][kafka] with no commit?
* A client using Kafka Streams crashing with an [**InvalidPidMappingException**][pid] after 7 days of inactivity?
* Starved events not being reported to monitoring since events *being delivered* would be considered a different state than *being in queue*?

<br/>
RTFM is really not enough.

[kstreams]: https://kafka.apache.org/documentation/streams/
[kafka]: https://cwiki.apache.org/confluence/display/KAFKA/KIP-186%3A+Increase+offsets+retention+default+to+7+days
[pid]: https://medium.com/@micaelaturrin/invalidpidmappingexception-issue-in-a-kafka-transactional-producer-ee1a6503bf31
