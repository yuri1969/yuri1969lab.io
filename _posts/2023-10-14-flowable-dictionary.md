---
layout: post
title:  "Flowable Dictionary"
categories: [workflows, flowable]
---

# Flowable Dictionary

Overwhelming...

* **Process Definition** - the [Flowable][flowable] internal representation of a BPMN 2.0 process - structure and behavior
* **Business Archive (BAR)** - BPMN 2.0 XML file(s) + supporting artifacts in a zip file
* **Deployment** - the unit of Flowable packaging  or an act of uploading and parsing XMLs to *Process Definition*
* **Deployment Version** - deployment of each *Process Definition* increases its version number (by XML definition ID)
* **Process Instance** - a started *Process Definition*
* **Process Diagram** - an image generated during *Deployment*
* **Activity** - a single unit of "work" done within business process
* **Task** - an atomic *Activity* - various types (User Task, Service Task, Script Task, etc.)
* **Sub-process** - a decomposable *Activity* (contains *Tasks* and other *Sub-processes*)
* **Start/End Event** - business process start/stop points
* **Sequence Flow** - an arrow connecting *Start/End Events* and *Activities*
* **Execution** - a pointer (BPMN 2.0 *Token*) following currently active activities by *Sequence Flow* within a *Process Instance* (a single instance consists of a tree of executions)
* **Process Variable** - data available through *Process Instance* lifetime
* **Local Variable** - data available only to a single *Execution*

[flowable]: https://www.flowable.com/open-source
