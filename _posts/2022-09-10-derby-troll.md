---
layout: post
title:  "Derby DB being a troll"
categories: [rant, db]
---

# Intro

- A *massive* legacy Java app being casually backed by [Apache Derby][derby] - a common deployment model of the app.
- Keeping daily Derby backups enabled since we are the good guys.
- Once a week making a full VM snapshot with all the services (including the DB) being shut down gracefully just to be sure.
- Four times a year performing a snapshot restore to prove its correctness.

# Once upon a time...

- At 00:30AM the regular VM snapshot failed and the services did not come back online.
- The Derby was apparently not up fast enough and the service refused to wait for it.
- Not a big deal, increase the waiting timeout and be cool. Restart all the things. Nope
- Repeat
- Notice the Derby service eats a whole CPU core immediately after start.
- Derby logs are empty. No updates done recently. Just a smooth legacy app ride.
- OK, some client surely performs a brutal query effectively killing the DB.
- So let's enable query logs.
- Nope, there are apparently no client queries being performed but still a whole CPU core is being eaten.
- Let's monitor who makes the initial TCP connection - the culprit client got identified.
- Prevent that client from starting.
- The DB runs smoothly. Cool
- Let's fire up a CLI client and perform a sanity check query.
- This immediately thorws the DB to the CPU hog state again.
- Again, no weird logs, nothing suspicious.
- This is Java. So somehow the Derby must be thrashing on GC.
- JMX to the rescue. Enable JMX and tunnel its port out just to find out that no GCs are being performed.
- So what are the Derby's threads doing? Just mundane listeners with one strange thread deep in **org.apache.derby.impl.store.raw.log.Scan.getNextRecord**.
- It turns out Derby DB utilizes a transaction log stored in its **log/** dir. Let's see - 14k transaction log data files.
- Thus it must be [scanning the log whole log detecting/repairing][derby_repair]. Fine, let's give it 30mins.
- Nah, how fast is it scanning the dir? Observing **/proc/\<PID\>/fd/** reveals it scans like 10 log files per second. Oh boy...
- Maybe the DB is fine, just the transaction log went mad. Let's nuke the log and start again (with a backup).
- Simply copy the *DB home dir* - well, with 30+GiB of small files it definitely takes some time inside a standard VM.
- Finally, the DB service starts. A basic sanity query works. Now let's start the client and be done with it.
- Nope, the client crashes with a really *internal* Derby Exception.
- There is no other way but wait for the DB to finish its painful repair job...
- After two hours of singe CPU crunching, the flock of transaction log files shrunk to a single one.
- The client service accepted this and so did I.

# Postmortem

The root cause: **hanging DB transactions prevented the DB from shutting down cleanly**

[derby]: https://db.apache.org/derby/
[derby_repair]: https://db.apache.org/derby/docs/10.0/manuals/develop/develop13.html

*[JMX]: Java Management Extensions
