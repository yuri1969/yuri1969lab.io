---
layout: post
title:  "Moving parts insanity"
categories: [k8s, workflows]
---

# Moving parts insanity

Nowadays an army of containers running on a bunch of nodes seems to be the *right solution* for anyone. Scaling, throughput, and reliability - all those are covered.

Employing such solution for processing workflows using a workflow engine based on a container orchestration technology sounds like a fit. Jobs running in a complete separation, *native* horizontal scaling, failover, etc. - this ticks all the shiny boxes. However, after a while one must dive deep into the nitty-gritty details of the technology stack. Then things start to get scary.

Distributed software is hard but this is a next level. Developing, managing, and operating those require really complex tooling, technology stack, and knowledge. Imagine yourself being woken up as an on-call in the middle of a night investigating what had broken down under the hood.

It's the amount of *moving parts* which makes such systems scary. Let's try to write down the major concepts.

## Kubernetes (k8s)

Container orchestration, deployment, and management; Google => CNCF; a cluster

* [**node**][node] - a machine (e.g. VM) running [CRI][cri] (e.g. Docker) runtime (hosting *containers*); [**kubelet**][kubelet] - agent managing containers' lifecycle; [**kube-proxy**][kubeproxy] - routes traffic to containers;
* [**pod**][pod] - a scheduling/deploying unit; holds 1-N containers which can share storage, address each other, and are scheduled on the same *node*;
* [**etcd**][etcd] - distributed key/value store; holds the whole k8s setup aka *resources*; acts as API server data source; 1MiB limit per resource
* **namespace** - split resources by environment, application, etc.
* [**volume**][volume] - ephemeral/persistent storage configurable within a pod; various types (e.g. NFS, GlusterFS, *emptyDir*); shared among containers in a pod; lifetime of a pod;
* [**persistent volume (PV)**][pv] - storage abstraction; exists independently from pods (lifecycle); can be dynamically created via *claim* (PVC);
* [**deployment**][deployment] - scheduling strategy changing the actual state to the desired one (described by deployment)
* [**job**][job] - scheduling strategy spawning 1-N pods attempting to successfully complete a task and terminate
* [**daemon sets**][daemon] - scheduling strategy assigning a pod to each node (log collection, monitoring, etc.)
* [**replica sets**][replicas] - scheduling strategy maintaining a defined number of pods available (a subset of deployment)
* [**stateful sets**][statefuls] - scheduling strategy maintaining ordering and uniqueness for *stateful pods* (MQ broker or database master/slave)
* [**service**][service] - discoverable set of pods addressable as one (e.g. service backends & round-robin)
* [**config maps/secrets**][configmaps] - config data available to pods; stored on a *master*; 1MiB limit
* **API server** - [*kube-apiserver*][apiserver]; JSON over HTTP frontend
* [**scheduler**][scheduler] - *kube-scheduler*; decides which unscheduled pod runs on which node; extensible
* [**controllers**][controller] - workers performing cluster changes; **job controller** - 1-N *pods* running to completion; **replication controller**; **daemon set controller**
* **controller manager** - *kube-controller-manager*; controllers' supervisor
* **cloud controller manager** - *cloud-controller-manager*; links the cluster to cloud provider APIs (react on cloud node deletion, maintain cloud LBs/IPs, etc.)
* **server API** - controls k8s objects withing a cluster; API server
* **operators API** - extensions to k8s; resources & controllers handling custom jobs (e.g. backup operations)
* **cluster API** - machines providing nodes are treated as k8s resources; cluster provider implements the API (e.g. cluster API calls AWS API); shape the cluster via *infra-as-software*
* **custom resource definition (CRD)** - [custom RESTful resources][cdr] stored in etcd; can be accessed from both cluster and CLI
* **service account** - allows assigning RBAC policies to pods; restrict the pods from accessing the k8s API
<br/>
Hey, there is even an official [glossary][glossary]...

## Serverless

Computing model trying to focus on the core functionality. Achieved via abstracting from - scaling, networking, *runtime*, packaging, management, etc.; scales down to 0 => no $$$

## Knative

Serverless-styled workloads on k8s

### Knative vs k8s

Knative:

* runs on k8s via CRD
* wraps the k8s functionality with a *serverless framework*
* tries to abstract from k8s details
* allows scaling to 0

### Architecture

[Services][knative-service] + [Events][knative-event]

Services are versioned, pulled from [OCI][oci] (Docker Registry); each got a route (a registered URL)
Events are emitted by services, routed via broker, filtered by triggers, and forwarded to other services

## Helm

A *packaging manager* for k8s; holds its [*charts*][helm-chart] in (OCI, Git) repository; a public repo exists; tracks status of k8s vs the chart;

### Helm vs k8s

Helm:

* calls k8s API
* uses Go templating to create k8s *templates*
* binds multiple k8s resources together in a chart
* packages the files and push/pull to/from a repo

### Architecture

Charts are pulled from a repo/registry by a Helm client and get transformed to k8s API calls.

## GitOps

A [CD][continous-deployment] methodology postulating:

* having a declarative way of describing the current running deployment;
* versioning the *deployment description* in a VCS;
* being able to deploy any difference between the described state and the current state in an automated way.

<br/>
This brings a nice CD, self-documentation of the whole landscape, shared knowledge about state, changes, and evolution.

### GitOps vs k8s

Repeatedly applied versioned k8s deployments + containerized apps => an ideal way to implement GitOps

### Tooling

* [Argo CD][argo-cd] (using Helm or other state description formats)
* [JenkinsX][jenkinsx] (set of k8s tools)
* [Terragrunt][terragrunt] (Terraform wrapper)
* [Flux][flux]

## Argo Workflows

A k8s-based workflow engine; a [Helm chart][argo-helm] is available;

### Argo Workflows vs k8s

Implemented as k8s CRD => brings security and interaction with the cluster.

### Architecture

* workflows defined as *workflow templates*; stored as k8s resources; cluster level vs namespace templates;
* cron workflows are workflows initiated by a cron expression (with all the cool features)
* workflows can create another workflows crudely via **curl** to Argo API...
* workflows are a more abstract kind of Kubernetes Jobs
* an artifact repository (AWS S3, Google GCS, JFrog Artifactory, MinIO etc.) intended for passing in/outs between WFs
* it is possible to use [volumes][argo-volume] and mount them in workflow templates
* inputs can be obtained from hardcoded Git, AWS S3, or Google GCS locations
* each k8s resource holds status of all nodes -> compressed nodes -> DB offloading - persistence to PostgreSQL/MySQL
* a workflow *status archive* - persistence to PostgreSQL/MySQL

[apiserver]: https://kubernetes.io/docs/reference/command-line-tools-reference/kube-apiserver/
[argo-cd]: https://argo-cd.readthedocs.io/en/stable/
[argo-helm]: https://github.com/argoproj/argo-helm/tree/main/charts/argo-workflows
[argo-volume]: https://argoproj.github.io/argo-workflows/walk-through/volumes/
[configmaps]: https://kubernetes.io/docs/concepts/configuration/configmap/
[continous-deployment]: https://en.wikipedia.org/wiki/Continuous_deployment
[controller]: https://kubernetes.io/docs/concepts/architecture/controller/
[cdr]: https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/
[cri]: https://kubernetes.io/docs/concepts/architecture/cri/
[daemon]: https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/
[deployment]: https://kubernetes.io/docs/concepts/workloads/controllers/deployment/
[etcd]: https://etcd.io/
[flux]: https://fluxcd.io/
[glossary]: https://kubernetes.io/docs/reference/glossary/?all=true
[jenkinsx]: https://jenkins-x.io/
[job]: https://kubernetes.io/docs/concepts/workloads/controllers/job/
[knative-service]: https://knative.dev/docs/serving/#serving-resources
[knative-event]: https://knative.dev/docs/eventing/
[kubelet]: https://kubernetes.io/docs/reference/command-line-tools-reference/kubelet/
[kubeproxy]: https://kubernetes.io/docs/reference/command-line-tools-reference/kube-proxy/
[helm-chart]: https://helm.sh/docs/topics/charts/
[node]: https://kubernetes.io/docs/concepts/architecture/nodes/
[oci]: https://github.com/opencontainers/distribution-spec/blob/main/spec.md
[pod]: https://kubernetes.io/docs/concepts/workloads/pods/
[pv]: https://kubernetes.io/docs/concepts/storage/persistent-volumes/
[replicas]: https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/
[service]: https://kubernetes.io/docs/concepts/services-networking/service/
[scheduler]: https://kubernetes.io/docs/concepts/scheduling-eviction/kube-scheduler/
[statefuls]: https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/
[terragrunt]: https://terragrunt.gruntwork.io/
[volume]: https://kubernetes.io/docs/concepts/storage/volumes/

*[on-call]: sleeping with your phone under the pillow
*[CNCF]: Cloud Native Computing Foundation
