---
layout: post
title:  "Technical Debt Story"
categories: [rant, sw]
---

# Changes are not easy

Got a bunch of **bash** scripts composing sort of a shared lib. The lib holds utils, custom system tools, etc. A common situation, right?

The library got gradually adopted at various places in our code base. Actually, it got adopted by various projects. Everything was cool and dandy. One day, an urgent request arrived - a new structure of the script files was desired.

- "So, moving scripts around, changing a few names... No problem, but these are breaking changes of the *lib's API*. You gotta adapt all your *clients*, you know."

- "What? Just add a symlink and we are good! It's just shell scripts."

- "Mkay. Everybody, plan a migration of your *clients* and don't use the old *API*. It's deprecated now!"

## A few years later

Finally, that old symlink hack had to go - symlinks and env vars don't fit together. Whole code base was scanned before the change - just to be sure.

There were more than 200 cases of calling the lib using the deprecated symlink in various ways...

**> Pay off your [debts][wiki] quickly!**

[wiki]:https://en.wikipedia.org/wiki/Technical_debt
