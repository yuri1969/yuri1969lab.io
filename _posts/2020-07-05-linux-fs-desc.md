---
layout: post
title:  "Linux FS desc"
categories: [linux, fs]
---

# Unix/Linux FS whys

Newcomers (especially *Windows ppl*) often dismiss the Unix FS hierarchy as something obnoxiously confusing. A singleton directory acting as a root sounds fine but some other design choices are truly not obvious at first glance.

It's like multiple dirs hosting binaries, a "user" directory holding just duplicated top level dirs, almost always composed of multiple partitions. OK

## Lineage

The 70's T&R Unix was developed on utterly constrained HW as explained in a [Rob Landley's post][busybox]. The original root FS was already composed of **/bin**, **/sbin**, **/lib**, or **/tmp**. Although **/usr** or **/home** were not present at that time. In a need of bigger disk space **/usr** was added and mounted as a "user" directory with the top level dir structure simply replicated.

Naturally, only non-essential stuff could go to the mounted **/usr**. Tools needed for boot were still required to be in **/bin**, etc.

Later the **/usr** got somehow replaced by today's user directory - **/home**. And yeah, also more exotic dirs like **/opt** appeared.

## Order of Today

In Linux world, *Filesystem Hierarchy Standard* (FSH) tries to standardize the hierarchy although major distros tend to employ slight differences using a custom set of rules/guidelines. For those who wonder, a universal manpage exists - [man 7 hier][hier].

Example of **bin** dir guidelines (based on [Gentoo's][gentoo]):

* **/bin** - boot-critical tools (eg. shell)
* **/sbin** - boot-critical system admin tools (eg. filesystem utils)
* **/usr/bin** - general tools governed by a distro (eg. Midnight Commander binary)
* **/usr/sbin** - non-critical system admin tools governed by a distro
* **/usr/local/bin** - fully user managed general tools (eg. manually compiled utils)
* **/usr/local/sbin** - fully user managed non-critical tools

## Why mounting so much?

There is often a tendency to spread the filesystem among different disk partitions and even physical drives. A classic filesystem hierarchy schema separates mounts for **/home**, **/var**, **/tmp** and others. Security is the main reason behind this.

A filled up root partition is dangerous since that state is rather unexpected so *bad things* can happen. Separating user-writable parts or dirs holding application generated logs from the root filesystem to different partitions (and/or setting quotas) helps to prevent that.

Filesystem consistency depends on reliability of underlying HW. In the hierarchy, there are areas with inherently frequent writes, like **/var/log** or **/tmp**. This fact should not affect the rest of the system even on the HW level. So separating those on a dedicated HW device makes sense. Furthermore, writes modify the filesystem which historically (mainly before adoption of [journaling][journal]) leaves a possibility for its corruption during *unexpected events*. This reaffirms the need for separating the frequently written areas from the root.

It's also possible to set [mount parameters][mount] disabling specific functionality for each filesystem. The parameters include options such as read-only mounting or effectively disabling SUID/SGID bits for the whole filesystem. For example, **/boot** have no need to be normally mounted as read-write or contain SUID-featured files. This way it's possible to make the attack surface smaller. Naturally, fine grained partitions bring more possibilities.

[busybox]:http://lists.busybox.net/pipermail/busybox/2010-December/074114.html
[hier]:https://linux.die.net/man/7/hier
[gentoo]:https://devmanual.gentoo.org/general-concepts/filesystem/
[journal]:https://en.wikipedia.org/wiki/Journaling_file_system
[mount]:https://linux.die.net/man/8/mount
