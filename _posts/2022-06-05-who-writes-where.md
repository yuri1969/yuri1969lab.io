---
layout: post
title:  "Filesystem auditing"
categories: [linux, fs]
---

# The spammer

Maintaining a legacy software often brings odd situations. One of those would be dealing with a *temp dir* flooded with thousands of files. Removing those files sounds trivial but identifying the real culprit is not. Moreover, when this situation happens in a big & entangled system consisting of many jobs and processes.

All those temp files were named in a rather generic fashion and, unfortunately, the majority of them was empty... So it wasn't possible to identify the source by content nor file name. *F.U.N*!

# The tooling

Touching the application(s) whacky code (introducing detailed logging, etc.) always sounds trickier than using an opaque/external solution. Finding a *PID* of the process creating one of those stalled temp files would do that.

There are many tools providing some kind of insight into various file-related operations, such as **lsof**, **inotify**, or even **strace**. There's also a dedicated FUSE-based utility called [LoggedFS][loggedfs]. Those tools are cool but they require to be installed/compiled on the target machine. Since the whole landscape we are talking about is RedHat-based, there has been [auditd][linux_audit] (*The Linux Audit*) present and running.

# auditd

The audit subsystem generates an audit log based on configured triggers - some of which are filesystem-related. The whole thing can be controlled via the **auditctl** tool or using a persisted form via the **/etc/audit/audit.rules** file.

A quick dig in the [manual][man] resulted in the following audit rule:

{% highlight bash %}
auditctl -a always,exit -S all -F dir=/the_evil_temp_dir -F filetype=file
{% endhighlight %}

It loosely translates as *"audit each syscall concenring a file inside the specified evil dir."*

After greatly increasing the audit log retention size within **/etc/audit/auditd.conf**, the hunt was about to begin.

# The hunt

The audit logs are written to **/var/log/audit/audit.log** and the used format is rather obvious so **grep** works well with it. After finding the line concerning one of the stalled temp files one needs to find the *PID* info within the correlated lines. This is done using the *audit event id* which is the string after the '**:**' char in **msg=audit(2113460534.329:5401113)**. After one more **grep** the spammer's *PID*, *PPID*, and other relevant attributes are revealed. That should be enough to identify the misbehaving process.

[loggedfs]: https://github.com/rflament/loggedfs
[linux_audit]: https://github.com/linux-audit
[man]: https://linux.die.net/man/8/auditctl
