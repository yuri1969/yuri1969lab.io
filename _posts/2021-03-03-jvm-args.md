---
layout: post
title:  "Handy JVM args"
categories: [java]
---

# Virtual Machine Jungle

It's a common knowledge that the Java VM accepts a rather huge number of parameters. It's possible to set mere default character encoding or tune all the fancy low-level VM knobs. The fact that the implemented set of parameters differs among JVM versions, vendors, and also the runtime platforms makes things a bit tedious. There are even [third-party sites][explorer] providing a filtered listing of available the parameters.

**sun.net.inetaddr.ttl** - Java features a DNS resolve cache which holds all the resolved IPs. This is a fairly common feature. However, some JVM implementations default their DNS cache entry TTL to infinity. So once the JVM successfully obtains a resolved IP for the requested name, it keeps using that cached IP until the JVM restarts. This effectively prevents DNS resolve changes for long running Java processes - servers. Overriding the default is even officially recommended for many workloads such as [AWS SDK][aws]. This particular property has been labeled as legacy but it still exists in recent JVMs ([OpenJDK17][17]). So having **-Dsun.net.inetaddr.ttl=60** set forces the TTL value to reasonable 60 seconds.

***OnOutOfMemoryError** - Current JVMs usually provide numerous flags and parameters to deal with the bane of [OoM][oom] states. The long-term favorite seems to be **HeapDumpOnOutOfMemoryError**, which simply dumps the heap to a binary file located in the working directory when the OoM gets hit. A quite flexible option is **OnOutOfMemoryError**, which is defined as **OnOutOfMemoryError=\'\<native command\>\'**. This one is often used to kill the JVM process on OoM via **-XX:OnOutOfMemoryError=\'kill -9 %p\'** or whatever else custom action is needed. The [JDK8u92][oracle8u92] release brought simple **ExitOnOutOfMemoryError** and **CrashOnOutOfMemoryError** flags which kill the JVM when the OoM is encountered. The latter also produces a fatal error log file. Using one of the JVM-killing flags together with a service auto-restart mechanism removes any potential OoM side effects.

## Useful linkz

- [Java Command Line Inspector][inspector]
- [VM Options Explorer][explorer]

[explorer]: https://chriswhocodes.com/vm-options-explorer.html
[aws]: https://docs.aws.amazon.com/sdk-for-java/latest/developer-guide/jvm-ttl-dns.html
[17]: https://github.com/openjdk/jdk/blob/jdk-17+9/src/java.base/share/classes/sun/net/InetAddressCachePolicy.java
[oom]: https://docs.oracle.com/javase/8/docs/technotes/guides/troubleshoot/memleaks002.html
[oracle8u92]: https://www.oracle.com/java/technologies/javase/8u92-relnotes.html
[inspector]: https://jacoline.dev/inspect
