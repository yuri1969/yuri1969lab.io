
#
# Makefile used to develop the site locally.
#
# Usage:
# make dev

dev: ; bundle exec jekyll serve --drafts --livereload --open-url
