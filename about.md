---
layout: page
title: /about
permalink: /about/
---

<ul class="social-media-list">
{% if site.gitlab_username %}
  <li>
    {% include icon-gitlab.html username=site.gitlab_username %}
  </li>
{% endif %}

{% if site.github_username %}
  <li>
    {% include icon-github.html username=site.github_username %}
  </li>
{% endif %}

{% if site.twitter_username %}
   <li>
     {% include icon-twitter.html username=site.twitter_username %}
   </li>
{% endif %}
</ul>
